# SSL Server Test

[![Downloads](https://img.shields.io/amo/users/qualys-ssl-server-test.svg)](https://addons.mozilla.org/en-US/firefox/addon/qualys-ssl-server-test/)
[![License](https://img.shields.io/badge/licensed-ethically-%234baaaa)](https://firstdonoharm.dev/)

Web extension that lets you quickly check the _hostname_ of the current tab or a link via the context menu against several services:
- [Blacklight](https://themarkup.org/blacklight/)
- [Blocklist Tools](https://blocklist-tools.developerdan.com)
- [Categorify](https://categorify.org)
- [crt.sh](https://crt.sh)
- [CryptCheck](https://tls.imirhil.fr/)
- [DNS Blacklist](https://dnsblacklist.org)
- [Hardenize](https://www.hardenize.com)
- [Internet.nl](https://en.internet.nl/)
- [KeyCDN](https://tools.keycdn.com/geo)
- [Mozilla Observatory](https://observatory.mozilla.org/)
- [Njalla](https://check.njal.la/dns)
- [Qualys SSL Labs](https://www.ssllabs.com/ssltest/)
- [Security Headers](https://securityheaders.com/)
- [Shodan](https://www.shodan.io)
- [SSL-Tools](https://ssl-tools.net/webservers/)
- [Tracking The Trackers](https://trackingthetrackers.com/)
- [urlscan.io](https://urlscan.io/)
- [VirusTotal](https://www.virustotal.com/)
- [Webbkoll](https://webbkoll.dataskydd.net/)
- [Whois.com](https://www.whois.com/whois/)
- [letsdebug.net](https://letsdebug.net)


## Download
* [Firefox](https://addons.mozilla.org/en-US/firefox/addon/qualys-ssl-server-test/)
* [Chromium](https://chrome.google.com/webstore/detail/ssl-server-test/kppeghgjfhkpgbnjoacjjmicedhdmfmb)

## Local Development Setup
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org)
	* [nvm](https://github.com/nvm-sh/nvm)
* Use specified Node version:
	* `nvm use`
* Install dependencies:
	* `npm i`
* Package for distribution:
	* `npm run bundle`

## Develop Locally in Firefox
* Run the extension in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) (open the [Browser Toolbox](https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox) for console logging):
	* `npm start`

## Develop Locally in Chromium
* Open Chromium and navigate to `chrome://extensions/`.
* Enabled "Developer mode" and select "Load unpacked."
* Open the `web-extension` directory to run the extension.
* Use the [Chrome Apps & Extensions Developer Tool](https://chrome.google.com/webstore/detail/chrome-apps-extensions-de/ohmmkhmmmpcnpikjeljgnaoabkaalbgc?utm_source=chrome-app-launcher-info-dialog) for debugging.


## Resources

<a target="_blank" href="https://icons8.com/icons/set/certificate">Certificate icon</a> by <a target="_blank" href="https://icons8.com">Icons8</a>